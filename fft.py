#!/usr/bin/env python3
import sys
import numpy as np
import matplotlib.pyplot as plt

# Read the data from the file
with open(sys.argv[1], 'r') as file:
    data = file.readline().strip().split(', ')
    data = np.array(data, dtype=int)

# Perform the Fast Fourier Transform (FFT)
fftResult = np.fft.fft(data)

# Calculate the frequency axis
samplingRate = 2 * np.max(np.abs(fftResult))
freq = np.fft.fftfreq(len(data), d=1/samplingRate)
print("Sampling Rate:", samplingRate, "[Hz]")

# Plot only the positive frequencies
positiveFreq = freq >= 0

# Calculate amplitude in decibels
amplitude = []

for value in np.abs(fftResult[positiveFreq]):
    if value != 0.0:
        amplitude.append(20*np.log10(value))
    else:
        amplitude.append(value)

amplitude = np.array(amplitude)

# Calculate the fundamental frequency
fundamentalIndex = np.argmax(amplitude)
fundamentalAmplitude = np.abs(amplitude[fundamentalIndex])
print("Fundamental Frequency Amplitude:", fundamentalAmplitude)

# Calculate harmonic frequencies
harmonicIndices = [fundamentalIndex + i for i in range(50)]
harmonicAmplitudes = np.abs(amplitude[harmonicIndices])
totalHarmonicAmplitude = 0
for i in range(50):
    if harmonicAmplitudes[i] != 0 and harmonicAmplitudes[i] != fundamentalAmplitude:
        print("Harmonic Frequency Amplitude:", harmonicAmplitudes[i])
        totalHarmonicAmplitude += harmonicAmplitudes[i]**2

# Calculate Total Harmonic Distortion THD
thd = np.sqrt(totalHarmonicAmplitude) / fundamentalAmplitude
print("Total Harmonic Distortion (THD):", thd)

# Create a new figure and adjust its size
plt.figure(figsize=(10, 6))

# Plot the FFT with a color gradient
plt.plot(freq[positiveFreq], amplitude, color='purple')

# Customize the plot aesthetics
plt.xlabel('Frequency [Hz]', fontsize=14)
plt.ylabel('Amplitude [dB]', fontsize=14)
plt.title('Spectrum Analyzer', fontsize=16)
plt.grid(True, linestyle='--', alpha=0.5)
plt.tick_params(axis='both', which='both', labelsize=12)

# Add a light color below the curve
plt.fill_between(freq[positiveFreq], amplitude, color='lavender', alpha=0.5)

# Show the plot
plt.tight_layout()
plt.show()
