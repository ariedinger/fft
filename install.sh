#!/usr/bin/env sh

print() {
    echo "$1" | tee -a log.install
}

CURRENT_DIR=$(pwd)

print "Setting root directory as $CURRENT_DIR"

touch $CURRENT_DIR/fft

print "Installing the fft script..."

cat > $CURRENT_DIR/fft <<EOF
#!/usr/bin/env bash
exec python3 "$(echo "$CURRENT_DIR")/fft.py" "\$@"
EOF

chmod +x $CURRENT_DIR/fft

mv $CURRENT_DIR/fft /bin/.

print "Checking if the fft script has been installed correctly..."

if command -v fft ; then
    print "  fft command has been successfully installed!"
else
    print "  ERROR ---> Installation of fft command failed."
    print "             Make sure you are running the installation script as sudo."
fi
